
//Tableau de mes compétences
var Skill = {
  'terminal' : {
    'nom':'Terminal',
    'pourcentage': 20,
    'logo':'<i class="fa-zoom fa fa-terminal fa-5x"></i>',
    'competences' : {
      'unlock':[
        'Linux',
        'Manipuler la console et les fichiers',
        'Nano',
        'bash'
      ],
      'lock': [
        'Contrôler les processus et les flux de données',
        'Automatiser les tâches avec des scripts',
        'Transférer des données à travers le réseau'
      ]
    }
  },
  'git' : {
    'nom':'GIT',
    'pourcentage': 60,
    'logo':'<i class="devicon-zoom devicon-git-plain"></i>',
    'competences' : {
      'unlock':[
        'Github',
        'Bitbucket',
        'Gerer et résoudre les conflits de merges'
      ],
      'lock': []
    }
  },
  'html' : {
    'nom' : 'HTML',
    'pourcentage' : 80,
    'logo':'<i class="fa-zoom fa fa-html5 fa-5x"></i>',
    'competences' : {
      'unlock':[
        'Architecture',
        'Formulaire'
      ],
      'lock': [
        'Web Acces',
        'Meta'
      ]
    }
  },
  'css' : {
    'nom':'CSS',
    'pourcentage': 40,
    'logo':'<i class="fa-zoom fa fa-css3 fa-5x"></i>',
    'competences' : {
      'unlock':[
        'Responsive',
        'Media Queries',
        'BootStrap',
      ],
      'lock': [
        'Fondation',
        'Animation css',
      ]
    }
  },
  'js' : {
    'nom':'Javascript',
    'pourcentage': 36,
    'logo':'<i class="devicon-zoom devicon-javascript-plain"></i>',
    'competences' : {
      'unlock':[
        'Jquery',
        'AngularJS',
        'Programmation Orientée Objet',
        'Ajax',
        'Expression Réguliéres',
      ],
      'lock': [
        'ReactJS',
        'NodeJS',
        'Angular 2'
      ]
    },
  },
  'php' : {
    'nom':'PHP',
    'pourcentage': 30,
    'logo':'<i class="devicon-zoom devicon-php-plain"></i>',
    'competences' : {
      'unlock':[
        'Symfony3',
        'Expressions Réguliéres',
        'Programmation Orientée Objet',
        'PDO',
      ],
      'lock': [
        'Cake PHP',
      ],
    },
  },
  'mysql':{
    'nom':'MYSQL',
    'pourcentage':30,
    'logo':'<i class="devicon-zoom devicon-mysql-plain"></i>',
    'competences': {
      'unlock':[
        'Ligne de Commande',
        'PhpMyAdmin',
        'Workbench'
      ],
      'lock' : []
    }
  },
  'photoshop' : {
    'nom':'Photoshop',
    'pourcentage': 50,
    'logo':'<i class="devicon-zoom devicon-photoshop-plain"></i>',
    'competences' : {
      'unlock':[
        'Créer et gérer des calques',
        'Retouches photos',
        'Réglages images',
        'Maitrise de la plume',
      ],
      'lock': []
    }
  },
  'ide' : {
    'nom':'IDE',
    'pourcentage': 60,
    'logo':'<i class="devicon-zoom devicon-atom-original"></i>',
    'competences' : {
      'unlock':[
        'Atom',
        'Netbean',
        'Notepad++',
        'SublimeText',
      ],
      'lock': []
    }
  },
}



$(document).ready(function(){
  $('.skills-detail').hide();
  var skill;
  // Affiche le detail d'une compétence au survol d'une icone
  $('.icone').mouseover(function(){
    skill = $(this).attr('skill');
    $('.skills-detail').show();
    //Charge le billet correspondant à l'attribut
    $('.skillName').text(Skill[skill]['nom']);
    $('.skill-progress-jauge').css('width',Skill[skill]['pourcentage']+'%');
    $('.skill-progress-jauge').css('transition','width 2s');
    $('.pourcent').text(Skill[skill]['pourcentage']+'%');
    $('.logo-skills-detail').html(Skill[skill]['logo']);
    $('.ul-maitrise').html('');
    $.each(Skill[skill]['competences']['unlock'],function( index, value){
      $('.ul-maitrise').append('<li class="unlocked"><i class="fa-li fa fa-dot-circle-o" aria-hidden="true"></i>'+value+'</li>');
      console.log(index + ' ' + value);
    });
    $.each(Skill[skill]['competences']['lock'],function( index, value){
      $('.ul-maitrise').append('<li class="locked"><i class="fa-li fa fa-lock" aria-hidden="true"></i>'+value+'</li>');
      console.log(index + ' ' + value);
    });
  });

  //Si clique en dehors de la zone de detail
  //Ou si scrolle plus bas


  //$('.skills-detail').hide();



});
